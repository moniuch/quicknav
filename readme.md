## jquery.fv.quicknav.js
quicknav is a jquery plugin written for a specific project and markup to facilitate navigation.

Upon hitting ctrl-G it opens up a div containing a site tree that can be easily filtered as you type-in to show matching items only.

**Defaults**

```
        dock: 'left',   // side of the window that the div should stick to
        tab: 'match',	// match = tabs through matching items only, all = tabs through all visible
        timeout: 700,	// [ms] time to elapse before assuming user stopped typing
        lang: 'en'		// locale
```