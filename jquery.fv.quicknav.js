/*
 Builds a filterable tree for type-in navigation.

 Expects static tree markup as nested ul/ol
 All necessary controls are built by jQuery

 @TODO fix responsiveness delay @ resetlink upon show
 @TODO signal typing mode?
 */

;(function ($) {
    'use strict';
    $.fn.highlight = function (str, className) {
        var regex = new RegExp(str, "gi");
        return this.each(function () {
            $(this)
                .contents()
                .filter(function () {
                    return this.nodeType == 3 && regex.test(this.nodeValue);
                })
                .replaceWith(function () {
                    return (this.nodeValue || "").replace(regex, function (match) {
                        return '<span class="' + className + '">' + match + '</span>';
                    });
                });
        });
    };
})(jQuery);

;(function ($, window, document, undefined) {
    'use strict';

    var MATCH_CLASS = 'match',
        HIGHLIGHT_CLASS = 'high',
        FOCUSED_CLASS = 'focused';

    var evns = '.fvqnav',
        nsInput = 'input' + evns + ' change' + evns,
        nsResize = 'resize' + evns,
        nsKeydown = 'keydown' + evns,
        nsKeyup = 'keyup' + evns,
        nsMousedown = 'mousedown' + evns,
        nsClick = 'click' + evns,
        nsBlur = 'blur' + evns,
        nsFocus = 'focus' + evns;

    var keyTimer;

    function FVQuicknav(element, options) {
        // try to get user locale. if not, load en defaults.
        var lang = options.lang || 'en';
        if(!$.fn.fvqnavtree.locale[lang]){
            lang = 'en';
        }
        var locale = $.extend($.fn.fvqnavtree.locale.en, $.fn.fvqnavtree.locale[lang]);

        this.settings = $.extend({}, $.fn.fvqnavtree.defaults, options);
        this.settings.locale = locale;

        this.$el = $(element);
        this.init();
//		return this;
    }

    FVQuicknav.prototype = {
        init: function () {
            var $el = this.$el,
                plugin = this;

            plugin.$hdr = $('<div class="quicknav-header quicknav-part"/>').prependTo($el);
            plugin.$bdy = $('<div class="quicknav-body quicknav-part"/>').appendTo($el);
            plugin.$ftr = $('<div class="quicknav-footer quicknav-part"/>').appendTo($el);
            plugin.$tree = $('.quicknav-tree', $el).appendTo(plugin.$bdy);
            plugin.$ctrls = $('<div class="quicknav-filter-controls"/>').prependTo(plugin.$hdr);

            plugin._shown = false;
            plugin._typing = false;

            // helpers
            var keyTimeout = 1e3,
                lastFilter = '';

            var filterTree = function($ul, needle) {
                if ($.trim(needle) == '') {
                    $('li', plugin.$tree).removeClass(MATCH_CLASS);
                }
                needle = needle.toLowerCase();

                if (!($ul.is('ul') || $ul.is('ol'))) {
                    return false;
                }

                var children = $ul.children('li'),
                    result = false;

                for (var i = 0, len = children.length; i < len; i++) {
                    var $li = $(children[i]),
                        $links = $li.find('>a'), // direct descendants links only
                        matching = false,
                        subch = $li.children().not('.ng-hide'),
                        sublen = subch.length;

                    if (subch.length > 0) {
                        for (var subi = 0; subi < sublen; subi++) {
                            var subMatching = filterTree($(subch[subi]), needle);
                            matching = matching || subMatching;
                        }
                    }

                    if (!matching) {
                        matching = $links.text().toLowerCase().indexOf(needle) > -1;
                    }

                    $links.highlight(needle, HIGHLIGHT_CLASS);
                    if($links.text().toLowerCase().indexOf(needle) > -1){
                        $li.addClass(MATCH_CLASS)
                    } else {
                        $li.removeClass(MATCH_CLASS);
                    }

                    $li.css({'display': matching ? '' : 'none'});
                    result = result || matching;
                }
                return result;
            };

            var _buildControls = function () {
                $el.removeClass('dock-left dock-right');
                if(plugin.settings.dock == 'right'){
                    $el.addClass('dock-right')
                } else {
                    $el.addClass('dock-left')
                }
                plugin.$status = $('<div class="quicknav-help"/>')
                    .appendTo(plugin.$ctrls);

                plugin.$typedinlabel = $('<span class="typedin"/>')
                    .appendTo(plugin.$status);

                plugin.$cmdlabel = $('<span class="quicknav-cmd">Find:</span>')
                    .insertBefore(plugin.$typedinlabel)
                    .text(plugin.settings.locale.help);

                plugin.$fld = $('<input class="quicknav-typedin-ghost" autofocus value=""/>')
                    .appendTo(plugin.$ctrls);

                plugin.$closebtn = $('<button class="close"/>')
                    .html('&times;')
                    .appendTo($el);

                plugin.$noresexpl = $('<p class="quicknav-no-result-explanation"/>').text(plugin.settings.locale.emptyResultExplanation);
                plugin.$resetbtn = $('<a href="#" class="quicknav-reset"/>').text(plugin.settings.locale.resetText),

                    plugin.$noresult = $('<div class="quicknav-no-result">')
                        .appendTo(plugin.$bdy)
                        .append(plugin.$noresexpl)
                        .append(plugin.$resetbtn);

                if(mCustomScrollbarAvailable()){
                    plugin.$bdy.mCustomScrollbar({
                        scrollInertia: 50,
                        mouseWheel: { preventDefault: true }
                    })
                }
            };

            var _armEvents = function () {
                $el
                    // falsing the clicks blocks hrefs entirely
                    .on(nsClick, function (e) {
                        // prevent input blurring
                        plugin._typefocus();
                        if($(e.target).is('.quicknav-tree a')){ // hide on nav click (not reset)
                            plugin.hide();
                        } else {
                            return false;
                        }
                    })
                    .on(nsKeydown, function (e) {
                        function kbdAdvanceTo(dir, $set, $start){
                            // dir = 'prev' || 'next'

                        }
                        switch(e.keyCode){
                            case 27: // esc
                                plugin.hide();
                                return false;

                            /*case 38: // up
                             var $matches = $('li.match > a', plugin.$tree),
                             $set,	// set of link elements to tab through
                             $f; 	// current focus

                             if(!$('li > a:focus', plugin.$tree).length){
                             $f = $('li > a', plugin.$tree).eq( $('li > a', plugin.$tree).length - 1).focus();
                             }

                             // if no matches - let browser overtake the keystroke
                             if($matches.length == 0) return true;

                             if(plugin.settings.tab == 'match'){
                             $set = $matches;
                             $f = $('li.match > a:focus', plugin.$tree);
                             } else {
                             $set = $('li:visible > a', plugin.$tree);
                             $f = $('li > a:focus', plugin.$tree);
                             }

                             var index = $set.index($f);

                             if(index > 0){
                             $set.eq(index-1).focus();
                             }

                             case 40: // down
                             var $matches = $('li.match > a', plugin.$tree),
                             $set,	// set of link elements to tab through
                             $f; 	// current focus

                             if(!$('li > a:focus', plugin.$tree).length){
                             $f = $('li > a', plugin.$tree).eq(0).focus();
                             }

                             // if no matches - let browser overtake the keystroke
                             if($matches.length == 0) return true;

                             if(plugin.settings.tab == 'match'){
                             $set = $matches;
                             $f = $('li.match > a:focus', plugin.$tree);
                             } else {
                             $set = $('li:visible > a', plugin.$tree);
                             $f = $('li > a:focus', plugin.$tree);
                             }

                             var index = $set.index($f);
                             if(index+1 < $set.length){
                             $set.eq(index+1).focus();
                             }
                             return false;*/

                            // conditionally preserve browser's default behavior
                            case 9: // tab

                                var $matches = $('li.match > a', plugin.$tree),
                                    $set,	// set of link elements to tab through
                                    $f; 	// current focus

                                // if no matches - let browser overtake the keystroke
                                if($matches.length == 0) return true;

                                if(plugin.settings.tab == 'match'){
                                    $set = $matches;
                                    $f = $('li.match > a:focus', plugin.$tree);
                                } else {
                                    $set = $('li:visible > a', plugin.$tree);
                                    $f = $('li > a:focus', plugin.$tree);
                                }

                                var index = $set.index($f);

                                if(e.shiftKey){ // shift-tab
                                    if(index > 0){
                                        $set.eq(index-1).focus();
                                    } else {
                                        $set.eq( $set.length-1 ).focus();
                                    }
                                } else {
                                    if(index+1 < $set.length){
                                        $set.eq(index+1).focus();
                                    } else {
                                        $set.eq(0).focus();
                                    }
                                }
                                return false;

                            // preserve browser's default behavior
                            case 3: // enter
                            case 13: // return
                                return true;

                            // any other keystrokes should reactivate typing-in
                            default:
                                plugin._typefocus();
                        }
                    })
                    .on(nsFocus, '.match a', function(e){
                        $(this).closest('li').addClass(FOCUSED_CLASS);
                    })
                    .on(nsBlur, '.match a', function(e){
                        $(this).closest('li').removeClass(FOCUSED_CLASS);
                    });


                plugin.$fld
                    .on(nsClick, function () {
                        return false;
                    })
                    .on(nsInput, function (e) { // filtering
                        var needle = $(this).val(),
                            $el = plugin.$el;
                        plugin.$typedinlabel.text(needle);
                        plugin.$cmdlabel.text(needle ? plugin.settings.locale.search : plugin.settings.locale.help);
                        filterTree(plugin.$tree, needle);

                        $('li > a').each(function(){
                            $(this).text($(this).text() );
                        });
                        if ($.trim(needle) == '') {
                            $('li', plugin.$tree).removeClass(MATCH_CLASS).show();
                        } else {
                            filterTree(plugin.$tree, needle);
                        }

                        var rescount = plugin.$tree.children(':visible').length;
                        if(rescount == 0){
                            plugin.$noresult.show();
                        } else {
                            plugin.$noresult.hide();
                        }
                        return false;
                    })
                    .on(nsKeyup, function(e){
                        clearTimeout(keyTimer);
                        keyTimer = setTimeout(function(){
                            plugin.$tree.find('li.' + MATCH_CLASS).first().find('>a').focus();
                            // console.log('stopped typing');
                            plugin.$el.trigger('stoppedtyping'+ evns);
                        }, plugin.settings.timeout);
                    })
                    .on(nsKeydown, function(e) {
                        plugin._typefocus();
                    })

                plugin.$closebtn
                    .on(nsClick, function (e) {
                        e.stopPropagation();
                        plugin.hide();
                    });

                plugin.$resetbtn
                    .on(nsClick, function(e){
                        e.preventDefault();
                        e.stopPropagation();
                        plugin.reset();
                    });

                $(window).on(nsResize, function(){
                    plugin.hide();
                });

                $(document)
                    .on(nsMousedown, function (e) {
                        if (!$(e.target).closest($el).length) {
                            plugin.hide();
                        }
                    });
            };

            _buildControls();
            _armEvents();
        },

        resize: function(){
            var plugin = this,
                $el = this.$el,
                treenoscrh = plugin.$tree.outerHeight(), // true height of the tree
                eh = $el.height(),
                tbpad = plugin.$bdy.outerHeight() - plugin.$bdy.height(),
                bhh = eh - plugin.$hdr.outerHeight() - plugin.$ftr.outerHeight() - tbpad;
            plugin.$bdy.height(bhh + 1); // +1 to prevent rounding errors


            $el
                .width($el.outerWidth())
                .height($el.outerHeight())

        },

        show: function() {
            var plugin = this,
                $el = this.$el;

            if(plugin._shown || $('#contextmenu-overlay').is(':visible')) return;

            plugin.reset();
            plugin.$bdy.height('');

            // Maintain height during filtering
            $el
                .width('')
                .height('')
                .show('fast', function () {
                    plugin._shown = true;
                    plugin.$fld.focus();
                    plugin.resize();
                });
        },

        hide: function () {
            var plugin = this,
                $el = this.$el;

            if(plugin._shown == false) return;
            $el.fadeOut('fast', function () {
                plugin._shown = false;
                plugin.reset();
                $el
                    .css('width', '')
                    .css('height', '')
            });
        },

        reset: function(){
            this.$fld.val('').change();
            this.$typedinlabel.text('');
            this.$noresult.hide();
            $('li', this.$el).removeClass(FOCUSED_CLASS);
            this._typefocus();
            clearTimeout(keyTimer);
        },

        _typefocus: function(){
            // re-focuses fld for typing
            this.$fld.focus();
            this._typing = true;
        }

//		_kbdLinkFocus: function
    };

    // Quick navigation tree
    $.fn.fvqnavtree = function (options, val) {
        return this.each(function () {
            if(typeof options === 'object'){
                if (undefined == $(this).data('fvqnavtree')) {
                    var plugin = new FVQuicknav(this, options);
                    $(this).data('fvqnavtree', plugin);
                }
            }
            else if(typeof options === 'string'){
                // allow to invoke public methods by name
                var plugin = $(this).data('fvqnavtree');
                if (!plugin || options.match(/^_/)) {
                    return;
                }
                plugin[options].call(plugin, val);
            }
        });
    }

    $.fn.fvqnavtree.defaults = {
        dock: 'left',
        tab: 'match',	// match = tabs through matching items only, all = tabs through all visible
        timeout: 700,	// [ms] time to elapse before assuming user stopped typing
        lang: 'en'		// get locale
    }

    // @TODO: extract to external i18n files
    $.fn.fvqnavtree.locale = {
        en: {
            help: 'Start typing to navigate...',
            emptyResultExplanation: 'No results found. The section you have typed in does not exist or you may not have been authorized to access it.',
            resetText: 'Reset search',
            search: 'Find:'
        },

        pl: {
            help: 'Zacznij pisa? by...'
        }
    };

    function mCustomScrollbarAvailable(){
        return (typeof $.fn.mCustomScrollbar === 'function');
    }

})(jQuery, window, document);

// INVOKE
$(function () {
    $('#quicknav').fvqnavtree({
        dock: 'left',
        tab: 'visible'
    })

    $(window).on('keydown', function (e) {
        if (e.keyCode == 71 && e.ctrlKey) {
            $('#quicknav').data('fvqnavtree').show();
            e.preventDefault();
        }
    });
});